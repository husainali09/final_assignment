//Download Functionality starts
function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;
        csvFile = new Blob([csv], {type: "text/csv"});
        downloadLink = document.createElement("a");
        //downloadLink.download = filename;
        downloadLink.setAttribute("download", filename);
        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);
        // Make sure that the link is not displayed
        downloadLink.style.display = "none";
        // Add the link to your DOM
        document.body.appendChild(downloadLink);
        // click to download file
        downloadLink.click();
      }

      function export_table_to_csv(html, filename) {
        var newfileName;
        var fileObj = document.getElementById("myFile").files[0]
        newfileName=fileObj.name;
        if(newfileName!=fileName){
          
          alert(messageConstants.checkFile);
          return false;
        }else{
          if(newfileName!=fileNameCalculator){
            alert(messageConstants.showCalculation);
          return false;
          }else{
            var csv = [];
            var rows = document.querySelectorAll("#myTable tr");
            for (var item = 0; item < rows.length; item++){
              var row = [], cols = rows[item].querySelectorAll("td, th");
              for (var colIndex = 0; colIndex < cols.length; colIndex++) 
                row.push(cols[colIndex].innerText);
            csv.push(row.join(","));    
          }
          // Download CSV
          download_csv(csv.join("\n"), filename);
          }
        }
      }
      //document.querySelector("#dnload").addEventListener("click", function () 
      function download()
      {
        var html = document.querySelector("#myTable").outerHTML;
        export_table_to_csv(html, "output_"+fileName);
      }