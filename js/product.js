//Displays File Name on TextBox
$('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('.custom-file-label').html(fileName);
});
var fileName;
var fileNameCalculator;
var productDetails = [];
function processFile() 
{   
    var fileObj = document.getElementById("myFile").files[0]
    if(fileObj==undefined) {
        alert(messageConstants.noFile);
        return false;
    }
    fileName=fileObj.name;
    productDetails = []; 
    var fileSize = document.getElementById('myFile').files[0].size;
    var theFile = document.getElementById("myFile").files[0];
    var fileInput= document.getElementById("myFile");
    var filePath= fileInput.value;    
    var allowedExtension = /(\.csv)$/i;
    if(!allowedExtension.exec(filePath)){
        alert(messageConstants.noCSV);
        fileInput.value='';
        return false; 
    }
   if(fileSize==0) 
   {
    alert(messageConstants.eFile);
    return false;
   }
    else{
        if (theFile){ 
            //var table = document.getElementById("myTable");
            var headerLine = "";
            var myReader = new FileReader();
            myReader.onload = function(e) {
                var content = myReader.result;
                var lines = content.split("\n");
                var headers = [];
                for (var count = 0; count < lines.length; count++){
                    var temp = {};
                    var rowContent = lines[count].split(",");
                    // for each line
                    for (var index = 0; index < rowContent.length; index++){
                     // first line
                    if(count === 0) {
                        headers.push(rowContent[index]);
                    } else {
                        // other lines
                        temp[headers[index]] = rowContent[index];
                    }
                    }
                    productDetails.push(temp);
                }
                var col = [];
                for (var index = 1; index < productDetails.length; index++) {
                    for (var key in productDetails[index]) {
                        if (col.indexOf(key) === -1) {
                            col.push(key);
                        }
                    }
                }
                if(col [0]=="Product" && col[1]=="Price" && col[2]=="Country"){
                // CREATE DYNAMIC TABLE.
                var table = document.createElement("table");
                // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
                var tr = table.insertRow(-1);                   // TABLE ROW.
                for (var i = 0; i < col.length; i++) {
                    var th = document.createElement("th");      // TABLE HEADER.
                    th.innerHTML = col[i];
                    tr.appendChild(th);
                }
                // ADD JSON DATA TO THE TABLE AS ROWS.
                for (var index = 1; index < productDetails.length -1; index++) {
                    tr = table.insertRow(-1);
                    for (var colIndex = 0; colIndex < col.length; colIndex++) {
                        if(isNaN(productDetails[index][col[2]])&&isNaN(productDetails[index][col[0]])&&(!isNaN(productDetails[index][col[1]]))){
                            var tabCell = tr.insertCell(-1);
                            tabCell.innerHTML = productDetails[index][col[colIndex]];
                        }else{
                            alert(messageConstants.invalidDataInFIle);
                            fileName=null;
                            return false;
                        }  
                    }
                }
                document.getElementById("message").innerHTML = fileName;
                // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
                var divContainer = document.getElementById("showData");
                divContainer.innerHTML = " ";
                divContainer.appendChild(table);
                document.getElementById("mybtn").style.display='inline';  
                }else {
                alert(messageConstants.invalidHeader);
                return false;
                }
            }
            myReader.readAsText(theFile);
        }
    }
return false;
}    
function calculator(){  
    var fileObj = document.getElementById("myFile").files[0]
    fileNameCalculator=fileObj.name;
    if (fileNameCalculator!=fileName){
        alert(messageConstants.checkFile);
        fileNameCalculator=null;
        return false;
    }
    document.getElementById("message2").innerHTML = fileName;
        var col = [];
        for (var index = 1; index < productDetails.length; index++) {
            for (var key in productDetails[index]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }        
    if(col [0]=="Product" && col[1]=="Price" && col[2]=="Country"){
        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
        var tr = table.insertRow(-1);                   // TABLE ROW.
        for (var index = 0; index < col.length; index++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[index];
            tr.appendChild(th);
            }
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = "Sales Tax";
        tr.appendChild(th);
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = "Sales Tax Amount";
        tr.appendChild(th);
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = "VAT";
        tr.appendChild(th);
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = "VAT Amount";
        tr.appendChild(th);
        var th = document.createElement("th");      // TABLE HEADER.
        th.innerHTML = "Final Price";
        tr.appendChild(th);
        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var index = 1; index < productDetails.length -1; index++) {
            tr = table.insertRow(-1);
            for (var colIndex = 0; colIndex < col.length; colIndex++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = productDetails[index][col[colIndex]];
            }   
            myTaxStrategy=new taxStrategy();
            var country=myTaxStrategy.countryFinder(productDetails[index][col[2]]);
            var costPrice= productDetails[index][col[1]]; 
            if(country=="INDIA"){
                india=new indiaTaxStrategy();
                var tax=india.tax();
                var vat=india.vat();        
                }else{
                    if(country=="ENGLAND"){
                        england=new englandTaxStrategy();
                        var tax=england.tax();
                        var vat=england.vat();
                        
                    }else{
                        if(country=="JAPAN"){
                            japan=new japanTaxStrategy();
                            var tax=japan.tax();
                            var vat=japan.vat();      
                        }else{
                            defaultTax=new defaultTaxStrategy();
                            var tax=defaultTax.tax();
                            var vat=defaultTax.vat();     
                        }
                    }
                }
            var taxAmount=addTaxAmount(costPrice,tax);
            var vatAmount=addVatAmount(costPrice,vat);
            var finalPrice=computeFinalPrice(costPrice,taxAmount,vatAmount);

            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = tax;
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = taxAmount;
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = vat;
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = vatAmount;
            var tabCell = tr.insertCell(-1);
            tabCell.innerHTML = finalPrice;
        }
        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("myTable");
        divContainer.innerHTML = " ";
        divContainer.appendChild(table);
        document.getElementById("dnload").style.display='inline';
    }else{
        alert(messageConstants.invalidHeader);
    }
}
function computeFinalPrice(price,tax,vat)
{
    var finalPrice=parseInt(price)+parseInt(tax)+parseInt(vat);
    return finalPrice;
}
function addTaxAmount(price,tax)
{
    var taxAmount=(parseInt(tax)/100)*parseInt(price);
    return taxAmount;

}
function addVatAmount(price,vat)
{
    var vatAmount=(parseInt(vat)/100)*parseInt(price);
    return vatAmount;

}
function sessionCreate(){
    let token_value =Math.random();
    sessionStorage.setItem("token",token_value);
}
