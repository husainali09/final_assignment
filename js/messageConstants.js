class messageConstants{
	static noFile="Please Select a CSV File";
	static noCSV='File is not in a csv format';
	static eFile="File is Empty";
	static invalidDataInFIle="File contains invalid data! Fix it then upload";
	static invalidHeader="Invalid CSV Format. Rearrange your file with proper headers as mentioned";
	static checkFile="First Check Your File";
	static showCalculation="Please Click On 'Show Calculation'";
}