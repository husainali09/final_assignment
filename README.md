# TAX CALCULATION WITH SOCIAL LOGIN
User Signup and Login with proper validations. Tax calculation by importing csv file.
Supports authentication with **Facebook**. Can be extended to other providers also.

Check out the [demo](https://localhost/project/template/index.html).

## Getting started

### Login Credentials

In your `WebApp`, import the `index.html`
User can login with socail login as well.

check [login](https://localhost/project/template/index.html)


### User Signup

Enter credentials of user with proper validations

check [login](https://localhost/project/template/signup.html)

### Social Signup

Use your social private details to sign in.

Install `XAMPP` server alongwith `Apache` server
Import your file on localhost in htdocs.
Integrate Social Login in javascript file.

### Import CSV and calculate tax

Import CSV file and calculate tax

Check [login](https://localhost/project/template/Product.html)

### Logout and clear session storage

Logging out will take you to the index page.


